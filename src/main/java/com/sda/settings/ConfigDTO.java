package com.sda.settings;

public class ConfigDTO {

    private String userName;
    private String localisation;
    private String calories;

    public ConfigDTO(String userName, String localisation, String calories) {
        this.userName = userName;
        this.localisation = localisation;
        this.calories = calories;
    }

    public String getUserName() {
        return userName;
    }

    public String getLocalisation() {
        return localisation;
    }

    public String getCalories() {
        return calories;
    }

}
