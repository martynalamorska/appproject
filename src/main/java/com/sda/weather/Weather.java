package com.sda.weather;

public class Weather {

    private Integer currentTemp;
    private Integer minTemp;
    private Integer maxTemp;
    private String weatherState;

    public Weather(Integer currentTemp, Integer minTemp, Integer maxTemp, String weatherState) {
        this.currentTemp = currentTemp;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.weatherState = weatherState;
    }

    public Integer getCurrentTemp() {
        return currentTemp;
    }

    public Integer getMinTemp() {
        return minTemp;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public String getWeatherState() {
        return weatherState;
    }

}
