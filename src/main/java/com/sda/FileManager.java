package com.sda;

import com.sda.settings.ConfigDTO;
import com.sda.settings.SettingsProperties;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class FileManager {

    public static ConfigDTO loadConfigProperties() {
        String username = null;
        String localisation = null;
        String calories = null;
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader("saves/config.properties");
            properties.load(fr);
            username = properties.getProperty(SettingsProperties.USERNAME.name());
            localisation = properties.getProperty(SettingsProperties.LOCALISATION.name());
            calories = properties.getProperty(SettingsProperties.CALORIES.name());
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ConfigDTO(username, localisation, calories);
    }

    public static void updateConfigProperties(ConfigDTO configDTO) {
        Properties properties = new Properties();
        try {
            FileWriter fw = new FileWriter("saves/config.properties");
            properties.setProperty(SettingsProperties.USERNAME.name(), configDTO.getUserName());
            properties.setProperty(SettingsProperties.LOCALISATION.name(), configDTO.getLocalisation());
            properties.setProperty(SettingsProperties.CALORIES.name(), String.valueOf(Double.valueOf(configDTO.getCalories()).intValue()));
            properties.store(fw, "");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
