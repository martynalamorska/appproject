package com.sda.main;

import com.sda.FileManager;
import com.sda.settings.SettingsProperties;
import com.sda.weather.Weather;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller implements Initializable {
private Logger logger = LogManager.getLogger(Controller.class);

    @FXML
    Parent mainPane;
    @FXML
    ScrollPane mainScrollPane;

    //welcome
    @FXML
    Label usernameLabel;

    //notes
    @FXML
    Label visibleNoteLabel;

    //calories
    @FXML
    PieChart caloriesPieChart;
    @FXML
    Label caloriesAbsorbedLabel;
    @FXML
    TextField newCaloriesTextField;

    //weather
    @FXML
    Label wLocalizationLabel;
    @FXML
    Label wTheTempLabel;
    @FXML
    Label wMinTempLabel;
    @FXML
    Label wMaxTempLabel;
    @FXML
    ImageView wImageView;

    //events
    @FXML
    ListView<String> eventsTodayListView;

    private Integer absorbed;
    private Integer caloriesLimit;
    private Weather weather;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loadData();

        mainScrollPane.prefWidthProperty().bind(((Pane) mainPane).widthProperty());
        mainScrollPane.prefHeightProperty().bind(((Pane) mainPane).heightProperty());

        eventsTodayListView.getItems().add("Mary's birthday.");
        eventsTodayListView.getItems().add("Annual meeting.");

        caloriesPieChart.setStartAngle(90);

        usernameLabel.setText(FileManager.loadConfigProperties().getUserName() + "!");

        ScheduledExecutorService caloriesUpdater =
                Executors.newSingleThreadScheduledExecutor();
        caloriesUpdater.scheduleAtFixedRate(new Runnable() {
            public void run() {
                loadData();
                updateCalories();
            }
        }, 0, 10, TimeUnit.MINUTES);

        ScheduledExecutorService weatherUpdater =
                Executors.newSingleThreadScheduledExecutor();
        weatherUpdater.scheduleAtFixedRate(new Runnable() {
            public void run() {
                weatherUpdate();
            }
        }, 0, 30, TimeUnit.MINUTES);

        weatherUpdate();


    }

    public void loadSettings(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("settings.fxml");
        if (resource != null) {
            try {
                Pane settingsPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(settingsPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadNotes(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("notes.fxml");
        if (resource != null) {
            try {
                Pane notesPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(notesPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadData() {

        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader("saves/calories.properties");
            properties.load(fr);
            LocalDate fromFile = LocalDate.parse(properties.getProperty(SettingsProperties.DATE.name()));
            if (fromFile.isEqual(LocalDate.now())) {
                absorbed = Integer.valueOf(properties.getProperty(SettingsProperties.ABSORBED.name()));
            } else {
                absorbed = 0;
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.caloriesAbsorbedLabel.setText(absorbed.toString());
        caloriesLimit = Integer.valueOf(FileManager.loadConfigProperties().getCalories());
        if (absorbed >= caloriesLimit) {
            ObservableList<PieChart.Data> caloriesData =
                    FXCollections.observableArrayList(
                            new PieChart.Data("Absorbed", caloriesLimit));
            this.caloriesPieChart.setData(caloriesData);
        } else {
            ObservableList<PieChart.Data> caloriesData =
                    FXCollections.observableArrayList(
                            new PieChart.Data("Absorbed", absorbed),
                            new PieChart.Data("Pending", caloriesLimit - absorbed));
            this.caloriesPieChart.setData(caloriesData);
        }
    }

    public void addCalories(MouseEvent mouseEvent) {
        absorbed += Integer.valueOf(newCaloriesTextField.getText());
        updateCalories();
        this.caloriesAbsorbedLabel.setText(absorbed.toString());
        if (absorbed >= caloriesLimit) {
            ObservableList<PieChart.Data> caloriesData =
                    FXCollections.observableArrayList(
                            new PieChart.Data("Absorbed", caloriesLimit));
            this.caloriesPieChart.setData(caloriesData);
        } else {
            ObservableList<PieChart.Data> caloriesData =
                    FXCollections.observableArrayList(
                            new PieChart.Data("Absorbed", absorbed),
                            new PieChart.Data("Pending", caloriesLimit - absorbed));
            this.caloriesPieChart.setData(caloriesData);
        }
    }

    private void updateCalories() {
        Properties properties = new Properties();
        try {
            FileWriter fw = new FileWriter("saves/calories.properties");
            properties.setProperty(SettingsProperties.ABSORBED.name(), absorbed.toString());
            properties.setProperty(SettingsProperties.DATE.name(), LocalDate.now().toString());
            properties.store(fw, "");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String loadJSONString(String url) {
        StringBuilder jsonText = new StringBuilder();
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(is, Charset.forName("UTF-8")));
            br.lines().forEach(jsonText::append);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return jsonText.toString();
    }


    public void weatherUpdate() {
        String localisationData = loadJSONString(
                "https://www.metaweather.com/api/location/search/?query="
                        + "warsaw");
        JSONArray localisationJSON = new JSONArray(localisationData);
        JSONObject weatherID = (JSONObject) localisationJSON.get(0);
        String woeid = weatherID.get("woeid").toString();

        JSONObject weatherJSON = new JSONObject(loadJSONString("https://www.metaweather.com/api/location/" +
                woeid));
        JSONArray consolidatedWeather =
                weatherJSON.getJSONArray("consolidated_weather");
        JSONObject weatherTodayJSON = consolidatedWeather.getJSONObject(0);

        weather = new Weather(weatherTodayJSON.getInt("the_temp"), weatherTodayJSON.getInt("min_temp"),
                weatherTodayJSON.getInt("max_temp"), weatherTodayJSON.get("weather_state_abbr").toString());

        wLocalizationLabel.setText(FileManager.loadConfigProperties().getLocalisation());
        wTheTempLabel.setText(weather.getCurrentTemp() + " °C");
        wMinTempLabel.setText(weather.getMinTemp() + " °C");
        wMaxTempLabel.setText(weather.getMaxTemp() + " °C");


        switch (weather.getWeatherState()) {
            case "sn":
            case "sl":
            case "h":
                wImageView.setImage(new Image("icons/weather-snow.png"));
                break;
            case "t":
                wImageView.setImage(new Image("icons/weather-storm.png"));
                break;
            case "hr":
            case "lr":
            case "s":
                wImageView.setImage(new Image("icons/weather-rain.png"));
                break;
            case "hc":
            case "lc":
                wImageView.setImage(new Image("icons/weather-clouds.png"));
                break;
            case "c":
                wImageView.setImage(new Image("icons/weather-sun.png"));
                break;
        }
    }


}