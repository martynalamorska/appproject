package com.sda.settings;

import com.sda.FileManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {
    private Logger logger = LogManager.getLogger(SettingsController.class);

    @FXML
    Parent settingsPane;

    @FXML
    TextField userName;

    @FXML
    TextField localisation;

    @FXML
    Slider caloriesSlider;

    @FXML
    Label caloriesLabel;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        caloriesSlider.setValue(Double.valueOf(FileManager.loadConfigProperties().getCalories()));
        userName.setText(FileManager.loadConfigProperties().getUserName());
        localisation.setText(FileManager.loadConfigProperties().getLocalisation());

        caloriesLabel.setText(String.valueOf(Double.valueOf(caloriesSlider.getValue()).intValue()));
        this.caloriesSlider.valueProperty().addListener(
                (observable, oldValue, newValue) -> caloriesLabel.setText(String.valueOf(newValue.intValue())));
    }

    public void closeSettings(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                FileManager.updateConfigProperties(new ConfigDTO(userName.getText(), localisation.getText(), caloriesLabel.getText()));
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) settingsPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
