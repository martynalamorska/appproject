package com.sda.settings;

public enum SettingsProperties {

    USERNAME, LOCALISATION, CALORIES, ABSORBED, DATE

}
