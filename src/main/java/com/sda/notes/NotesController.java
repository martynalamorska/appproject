package com.sda.notes;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NotesController implements Initializable {

    @FXML
    Parent notesPane;

    @FXML
    FlowPane flowPane;

    @FXML
    Label addNote;

    @FXML
    TextArea newNote;


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        addNewNote();
    }

    public void closeNotes(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) notesPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void addNewNote(){ //MouseEvent mouseEvent
        Pane pane = new Pane();
        pane.setPrefWidth(100.0);
        pane.setPrefHeight(100.0);
        pane.setStyle("-fx-background-color: white");
        flowPane.getChildren().add(pane);
        //flowPane.getChildren().remove(pane);
    }
}
